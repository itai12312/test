from __future__ import print_function
import argparse
import collections
import os
import string

import azure.storage.blob as azureblob

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--filepath', required=True,
                        help='The path to the text file to w. The path'
                             'may include a compute node\'s environment'
                             'variables, such as'
                             '$AZ_BATCH_NODE_SHARED_DIR/filename.txt')
    #parser.add_argument('--numwords', type=int, required=True,
    #                    help='The number of words to print top frequency.')
    parser.add_argument('--storageaccount', required=True,
                        help='The name the Azure Storage account that owns the'
                             'blob storage container to which to upload'
                             'results.')
    parser.add_argument('--storagecontainer', required=True,
                        help='The Azure Blob storage container to which to'
                             'upload results.')
    parser.add_argument('--sastoken', required=True,
                        help='The SAS token providing write access to the'
                             'Storage container.')
    args = parser.parse_args()

    #input_file = os.path.realpath(args.filepath)
    output_file = args.filepath #'{}_OUTPUT{}'.format(
        #os.path.splitext(args.filepath)[0],
        #os.path.splitext(args.filepath)[1])

    #with open(input_file) as f:
    #    words = [word.strip(string.punctuation) for word in f.read().split()]

    #word_counts = collections.Counter(words)
    with open(output_file, "w") as text_file:
        print('Word\tCount', file=text_file)
        print("------------------------------", file=text_file)
        #for word, count in word_counts.most_common(args.numwords):
        #    print(word + ':\t' + str(count), file=text_file)
        print("------------------------------", file=text_file)
        print("Node: " + os.environ['AZ_BATCH_NODE_ID'], file=text_file)
        print("Task: " + os.environ['AZ_BATCH_TASK_ID'], file=text_file)
        print("Job:  " + os.environ['AZ_BATCH_JOB_ID'], file=text_file)
        print("Pool: " + os.environ['AZ_BATCH_POOL_ID'], file=text_file)

    # Create the blob client using the container's SAS token.
    # This allows us to create a client that provides write
    # access only to the container.
    blob_client = azureblob.BlockBlobService(account_name=args.storageaccount,
                                             sas_token=args.sastoken)

    output_file_path = os.path.realpath(output_file)

    print('Uploading file {} to container [{}]...'.format(
        output_file_path,
        args.storagecontainer))

    blob_client.create_blob_from_path(args.storagecontainer,
                                      output_file,
                                      output_file_path)
    

"""import numpy as np
import pandas as pd

import tensorflow as tf
from tensorflow.contrib import learn

from model import cnn_model_fn
from dataset import InputFuncs


tf.logging.set_verbosity(tf.logging.DEBUG   )


def main(unused_argv):
    # Load training and eval data
    data_store = 'data/signals.hash.hdf5'
    gt = pd.read_csv('data/formatted_maagar_device_ids.csv').set_index(
        'deviceID')
    test_ids_path = 'data/test_ids.csv'
    labels_series = gt['gender']
    n_categories = len(labels_series.unique())

    input_funcs = InputFuncs(data_store, labels_series, min_signals=100, max_signals=1000,
                             test_ids_path=test_ids_path , batch_size=100, num_epochs=10000)

    # Create the Estimator
    mnist_classifier = learn.Estimator(
        model_fn=cnn_model_fn, params={'n_categories': n_categories}, model_dir="/tmp/tf_model",
        config=learn.RunConfig(save_checkpoints_steps=10))

    tensors_to_log = {#"probabilities": "softmax_tensor",
                      "loss": "loss"}

    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=1)

    validation_metrics = {
        "accuracy":
            tf.contrib.learn.MetricSpec(
                metric_fn=tf.contrib.metrics.streaming_accuracy,
                prediction_key=tf.contrib.learn.PredictionKey.CLASSES),
        "precision":
            tf.contrib.learn.MetricSpec(
                metric_fn=tf.contrib.metrics.streaming_precision,
                prediction_key=tf.contrib.learn.PredictionKey.CLASSES),
        "recall":
            tf.contrib.learn.MetricSpec(
                metric_fn=tf.contrib.metrics.streaming_recall,
                prediction_key=tf.contrib.learn.PredictionKey.CLASSES)
    }

    validation_monitor = learn.monitors.ValidationMonitor(
        input_fn=input_funcs.test_input_fn, every_n_steps=1, metrics=validation_metrics)

    mnist_classifier.fit(
        input_fn=input_funcs.train_input_fn, monitors=[logging_hook,
                                                       validation_monitor])

    # Configure the accuracy metric for evaluation
    metrics = {
        "accuracy":
            learn.MetricSpec(
                metric_fn=tf.metrics.accuracy, prediction_key="classes"),
    }

    # Evaluate the model and print results
    eval_results = mnist_classifier.evaluate(
        input_fn=input_funcs.test_input_fn, metrics=metrics)
    print(eval_results)




if __name__ == "__main__":
    tf.app.run()
"""